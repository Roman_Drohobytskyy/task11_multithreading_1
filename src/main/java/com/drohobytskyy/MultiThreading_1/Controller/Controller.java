package com.drohobytskyy.MultiThreading_1.Controller;

import com.drohobytskyy.MultiThreading_1.Model.FibonacciRunnable;
import com.drohobytskyy.MultiThreading_1.Model.PingPong;
import com.drohobytskyy.MultiThreading_1.Model.Scheduled;
import com.drohobytskyy.MultiThreading_1.Model.SynchronizationOfThreeMethods;
import com.drohobytskyy.MultiThreading_1.View.ConsoleView;

import java.io.IOException;

public class Controller {
    private final static String DIVIDER = "----------------------------------------------------------------------" +
            "-----------------------------------------------------------------";
    private ConsoleView view;
    public PingPong pingPong;
    FibonacciRunnable fibonacciRunnable;
    Scheduled scheduled;
    SynchronizationOfThreeMethods synchronization;
//    private TestClass testClass;
//    private CallingClass callingClass;

    public Controller(ConsoleView view) throws IOException {
        this.view = view;
        view.controller = this;
        pingPong = new PingPong();
        fibonacciRunnable = new FibonacciRunnable();
        fibonacciRunnable.view = view;
        scheduled = new Scheduled();
        scheduled.view = view;
        synchronization = new SynchronizationOfThreeMethods();
        synchronization.view = view;
//        this.testClass = testClass;
//        this.callingClass = callingClass;
//        testClass.view = view;
//        callingClass.view = view;
//        callingClass.testClass = testClass;
//        callingClass.execute();
        view.createMenu();
        view.executeMenu(view.menu);
    }

    public void runPingPong() {
        view.logger.warn(DIVIDER);
        view.logger.info(pingPong.show());
    }

    public void printSequenceOfFibonacciNumbers() {
        view.logger.warn(DIVIDER);
        fibonacciRunnable.printSequenceOfFibonacciNumbers();
    }

    public void printCachedThreadPool() {
        view.logger.warn(DIVIDER);
        fibonacciRunnable.printCachedThreadPool();
    }

    public void printFixedThreadPool() {
        view.logger.warn(DIVIDER);
        fibonacciRunnable.printFixedThreadPool();

    }

    public void printSingleThreadExecutor() {
        view.logger.warn(DIVIDER);
        fibonacciRunnable.printSingleThreadExecutor();
    }

    public void printScheduledThreadPool() {
        view.logger.warn(DIVIDER);
        scheduled.printScheduledThreadPool();
    }

    public void showSynchronizedOnSingleObject() {
        view.logger.warn(DIVIDER);
        synchronization.showSynchronizedOnSingleObject();
    }

    public void showSynchronizedOnDifferentObjects() {
        view.logger.warn(DIVIDER);
        synchronization.showSynchronizedOnDifferentObjects();
    }
}
