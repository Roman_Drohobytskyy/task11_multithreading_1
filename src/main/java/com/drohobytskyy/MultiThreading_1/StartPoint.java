package com.drohobytskyy.MultiThreading_1;

import com.drohobytskyy.MultiThreading_1.Controller.Controller;
import com.drohobytskyy.MultiThreading_1.View.ConsoleView;

import java.io.IOException;

public class StartPoint {

    public static void main(String[] args) {
        {
            try {
                Controller controller = new Controller(new ConsoleView());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
