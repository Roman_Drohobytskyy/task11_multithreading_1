package com.drohobytskyy.MultiThreading_1.View;

import com.drohobytskyy.MultiThreading_1.Controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;

public class ConsoleView {
    public final static String EXIT = "exit";
    private BufferedReader input;
    public Controller controller;
    public Map<String, MenuItem> menu;
    public Logger logger = LogManager.getLogger(ConsoleView.class);

    public ConsoleView() throws IOException {
        input = new BufferedReader(new InputStreamReader(System.in));
    }

    public void createMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", new MenuItem(" 1   - Ping Pong.",
                controller::runPingPong));
        menu.put("2", new MenuItem(" 2   - Print a sequence of n Fibonacci numbers.",
                controller::printSequenceOfFibonacciNumbers));
        menu.put("3", new MenuItem(" 3   - CachedThreadPool.",
                controller::printCachedThreadPool));
        menu.put("4", new MenuItem(" 4   - FixedThreadPool.",
                controller::printFixedThreadPool));
        menu.put("5", new MenuItem(" 5   - SingleThreadExecutor.",
                controller::printSingleThreadExecutor));
        menu.put("6", new MenuItem(" 6   - Create a task that sleeps for a random amount of time between " +
                "1 and 10 seconds using ScheduledThreadPool.",
                controller::printScheduledThreadPool));
        menu.put("7", new MenuItem(" 7   - Synchronization of three methods on one object.",
                controller::showSynchronizedOnSingleObject));
        menu.put("8", new MenuItem(" 8   - Synchronization of three methods on multiple objects.",
                controller::showSynchronizedOnDifferentObjects));
        menu.put(EXIT, new MenuItem(EXIT + " - exit from app", this::exitFromMenu));
    }

    public void executeMenu(Map<String, MenuItem> menu) throws IOException {
        String keyMenu;
        do {
            logger.warn("-------------------------------------------------------------------------------------"
                    + "-------------------------------------------------");

            logger.warn("Please, select menu point.");
            outputMenu(menu);
            keyMenu = input.readLine().toLowerCase();
            try {
                menu.get(keyMenu).getLink().print();
            } catch (Exception e) {
                logger.error("Invalid input! Please try again.");
            }
        } while (true);
    }

    private void outputMenu(Map<String, MenuItem> menu) {
        for (MenuItem pair : menu.values()) {
            logger.info(pair.getDescription());
        }
    }

    public void exitFromMenu() {
        logger.warn("Have an amazing day!");
        System.exit(0);
    }

}

