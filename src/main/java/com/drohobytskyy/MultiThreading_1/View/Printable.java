package com.drohobytskyy.MultiThreading_1.View;

import java.io.IOException;

@FunctionalInterface
public interface Printable {

    void print() throws IOException;
}

