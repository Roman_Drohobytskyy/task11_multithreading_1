package com.drohobytskyy.MultiThreading_1.Model;

import com.drohobytskyy.MultiThreading_1.View.ConsoleView;

import static java.lang.Thread.sleep;

public class SynchronizationOfThreeMethods {
    public ConsoleView view;
    String obj = "";
    Object obj1 = new String("");
    String obj2 = new String("");
    String obj3 = new String("");

    //private static Object obj = new Object();

    private void changeToFirst (String s) {
        synchronized (s) {
            try {
                sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            s = "First";
            view.logger.info("String obj = " + s);
        }
    }

    private void changeToSecond (String s) {
        synchronized (s) {
            try {
                sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            s = "Second";
            view.logger.info("String obj = " + s);
        }
    }

    private void changeToThird(String s) {
        synchronized (s) {
            try {
                sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            s = "Third";
            view.logger.info("String obj = " + s);
        }
    }

    private void changeToFirstDifferent (String s) {
        synchronized (obj1) {
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            s = "First";
            view.logger.info("String obj = " + s);
        }
    }

    private void changeToSecondDifferent (String s) {
        synchronized (obj2) {
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            s = "Second";
            view.logger.info("String obj = " + s);
        }
    }

    private void changeToThirdDifferent(String s) {
        synchronized (obj3) {
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            s = "Third";
            view.logger.info("String obj = " + s);
        }
    }

    public void showSynchronizedOnSingleObject() {
        new Thread(() -> changeToFirst(obj)).start();
        new Thread(() -> changeToSecond(obj)).start();
        new Thread(() -> changeToThird(obj)).start();
        try {
            sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void showSynchronizedOnDifferentObjects() {
        new Thread(() -> changeToFirstDifferent(obj)).start();
        new Thread(() -> changeToSecondDifferent(obj)).start();
        new Thread(() -> changeToThirdDifferent(obj)).start();
        try {
            sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
