package com.drohobytskyy.MultiThreading_1.Model;

import com.drohobytskyy.MultiThreading_1.View.ConsoleView;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FibonacciRunnable {
    public ConsoleView view;
    private ExecutorService pool;

    public void printSequenceOfFibonacciNumbers() {
        view.logger.warn("Exercise 2: ");
        for (int i = 5; i < 7; i++) {
            new Thread(new FibonacciTask(i)).start();
        }
    }

    public void printCachedThreadPool() {
        view.logger.warn("CachedThreadPool: ");
        pool = Executors.newCachedThreadPool();
        for (int i = 5; i < 8; i++) {
            pool.execute(new FibonacciTask(i));
        }
        pool.shutdown();
    }

    public void printFixedThreadPool() {
        System.out.println("FixedThreadPool: ");
        pool = Executors.newFixedThreadPool(3);
        for (int i = 5; i < 8; i++) {
            pool.execute(new FibonacciTask(i));
        }
        pool.shutdown();
    }

    public void printSingleThreadExecutor() {
        view.logger.warn("SingleThreadExecutor: ");
        pool = Executors.newSingleThreadExecutor();
        for (int i = 5; i < 8; i++) {
            pool.execute(new FibonacciTask(i));
        }
        pool.shutdown();
    }
}

class FibonacciTask implements Runnable {
    private final int count;
    private static Object sync = new Object();

    FibonacciTask(int count) {
        this.count = count;
    }

    @Override
    public void run() {
        StringBuffer stringBuffer = new StringBuffer();
        int sum = 0;
        synchronized (sync) {
            for (int i = 0; i < count; i++) {
                stringBuffer.append(fibonacci(i) + " ");
                sum += fibonacci(i);
            }
            System.out.println(stringBuffer.toString() + " ---> sum = " + sum);
        }
    }

    private int fibonacci(int n) {
        if (n == 0 || n == 1) {
            return 1;
        } else {
            return fibonacci(n - 1) + fibonacci(n - 2);
        }
    }
}