package com.drohobytskyy.MultiThreading_1.Model;

public class PingPong {

    private volatile static StringBuilder pingPongResult = new StringBuilder();
    private static Object sync = new Object();
    private static final int COUNT_OF_PRINTING = 3;

    public static void main(String[] args) {
        System.out.println(new PingPong().show());
    }

    private void sayPing() {
        synchronized (sync) {
            for (int i = 0; i < COUNT_OF_PRINTING; i++) {
                sync.notify();
                try {
                    sync.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                pingPongResult.append("ping ");
                sync.notify();
            }
        }
    }

    private void sayPong() {
        synchronized (sync) {
            for (int i = 0; i < COUNT_OF_PRINTING; i++) {
                sync.notify();
                try {
                    sync.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                pingPongResult.append("pong ");
                sync.notify();
            }
        }
    }

    public String show() {
        pingPongResult = new StringBuilder();
        Thread t1 = new Thread(this::sayPing);
        Thread t2 = new Thread(this::sayPong);
        t1.start();
        t2.start();
        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return pingPongResult.toString();
    }
}
