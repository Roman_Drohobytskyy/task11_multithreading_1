package com.drohobytskyy.MultiThreading_1.Model;


import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.Random;

import com.drohobytskyy.MultiThreading_1.View.ConsoleView;
import org.joda.time.DateTime;
import org.joda.time.Interval;

public class Scheduled {
    public static ConsoleView view;

    public void printScheduledThreadPool() {
        ScheduledExecutorService scheduledThreadPool = Executors.newScheduledThreadPool(5);

        //schedule to run after sometime
        view.logger.warn("Current Time = "+new Date());
        for(int i=0; i<3; i++){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            WorkerThread worker = new WorkerThread("do heavy processing", view);
            scheduledThreadPool.schedule(worker, 1, TimeUnit.SECONDS);
        }

        scheduledThreadPool.shutdown();
        while(!scheduledThreadPool.isTerminated()){
            //wait for all tasks to finish
        }
        view.logger.warn("Finished all threads");
    }
}

class WorkerThread implements Runnable{
    ConsoleView view = Scheduled.view;
    Random random = new Random();
    private String command;

    public WorkerThread(String s, ConsoleView view){
        this.command=s;
    }

    @Override
    public void run() {
        DateTime start = new DateTime();
        view.logger.info(Thread.currentThread().getName()+" Start. Time = "
                + start.getHourOfDay() + ":" + start.getMinuteOfHour() + ":" + start.getSecondOfMinute());
        processCommand();
        DateTime finish = new DateTime();
        Interval interval = new Interval(start, finish);
        view.logger.info(Thread.currentThread().getName()+ " End.   Time = "
                + finish.getHourOfDay() + ":" + finish.getMinuteOfHour() + ":" + finish.getSecondOfMinute()
                + " ----> Slept time: " + interval.toDuration().getStandardSeconds());
    }

    private void processCommand() {
        try {
            int timeToSleep = random.nextInt(10) + 1;
            Thread.sleep(timeToSleep * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}